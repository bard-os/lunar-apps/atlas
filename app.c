/*
 * Atlas 
 *
 * Copyright (C) 2022, Kibomo, Inc.
 *
 * Atlas is free software; you can redistribute it and/or  
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either Version 3 of the 
 * License, or (at your option) any later version.
 *
 * Atlas is distributed in hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Parker Ritchie <parker@bard-os.org> 
 *
 */

#include <gtk/gtk.h>
#include <app.h>
#include <window.h>

/* app: the main atlas class */

struct _AtlasApp
{
  GtkApplication parent;
}

G_DEFINE_TYPE(AtlasApp, atlas_app, GTK_TYPE_APPLICATION);

static void
atlas_app_activate (Gapplication *app)
{
  AtlasAppWindow *win;

  win = atlas_app_window_new (ATLAS_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void 
atlas_app_init (AtlasApp *app)
{
}

static void
atlas_app_class_init (AtlasAppClass *class)
{
  G_APPLICATION_CLASS (class)->startup = atlas_app_startup;
  G_APPLICATION_CLASS (class)->activate = atlas_app_activate;
  G_APPLICATION_CLASS (class)->open = atlas_app_open;
}

AtlasApp * 
atlas_app_new (void)
{
  return g_object_new (ATLAS_APP_TYPE, 
		      "application-id", "org.bard-os.atlas",
		      "flags, G_APPLICATION_HANDLES_OPEN,
		      NULL);
}
