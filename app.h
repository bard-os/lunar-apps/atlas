/*
 * Atlas 
 *
 * Copyright (C) 2022, Kibomo, Inc.
 *
 * Atlas is free software; you can redistribute it and/or  
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either Version 3 of the 
 * License, or (at your option) any later version.
 *
 * Atlas is distributed in hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Parker Ritchie <parker@bard-os.org> 
 *
 */

#include <gtk/gtk.h>

#ifndef _ATLASAPP_H
#define _ATLASAPP_H

#define ATLAS_APP_TYPE (atlas_app_get_type ())
G_DECLARE_FINAL_TYPE (AtlasApp, atlas_app, ATLAS, APP, GtkApplication)

AtlasApp *atlas_app_new (void);

#endif /* _ATLASAPP_H */
